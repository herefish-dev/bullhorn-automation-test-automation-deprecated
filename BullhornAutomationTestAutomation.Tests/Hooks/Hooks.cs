﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace BullhornAutomationTestAutomation.Tests.Hooks;

[Binding]
public sealed class Hooks
{
    // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

    private readonly IObjectContainer _container;
    private readonly ScenarioContext _scenarioContext;

    public Hooks(IObjectContainer container, ScenarioContext scenarioContext)
    {
        _container = container;
        _scenarioContext = scenarioContext;
    }

    [BeforeScenario]
    public void BeforeScenario()
    {
        var options = new ChromeOptions();
        options.AddArguments( "chrome.switches", "--disable-extensions --headless --disable-extensions-file-access-check --disable-extensions-http-throttling --disable-infobars --enable-automation --start-maximized");
        options.AddUserProfilePreference("credentials_enable_service", false);
        options.AddUserProfilePreference("profile.password_manager_enabled", false);
        IWebDriver driver = new ChromeDriver(options);
        driver.Manage().Window.Maximize();
        _container.RegisterInstanceAs(driver);
        driver.Url = "https://test-herefish-web.azurewebsites.net/";
        //"--headless",
    }

    //[BeforeScenario("@Prod")]
    //public void BeforeScenarioProd()
    //{
    //    IWebDriver driver = new ChromeDriver();
    //    driver.Manage().Window.Maximize();
    //    _container.RegisterInstanceAs<IWebDriver>(driver);
    //    driver.Url = "https://app.herefish.com/Account/Login";

    //}

    [AfterScenario]
    public void AfterScenario()
    {
        var driver = _container.Resolve<IWebDriver>();
        driver?.Quit();
    }
}
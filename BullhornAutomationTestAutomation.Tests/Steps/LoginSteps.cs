﻿using System.Threading;
using BullhornAutomationTestAutomation.Tests.PageObjects;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BullhornAutomationTestAutomation.Tests.Steps;

[Binding]
public sealed class LoginSteps
{
    private IWebDriver _driver;
    readonly LoginPage _loginPage;
    readonly HomePage _homePage;

    public LoginSteps(IWebDriver driver)
    {
        _driver = driver;
        _loginPage = new LoginPage(driver);
        _homePage = new HomePage(driver);
    }

    [StepDefinition("I navigate to Herefish")]
    public void GivenINavigateToHerefish()
    {
        //driver = new ChromeDriver();
        //driver.Url = "https://test-herefish-web.azurewebsites.net/";
    }
        
    [StepDefinition("I enter valid Username")]
    public void GivenIEnterValidUsername()
    {
        _loginPage.EnterUsername("arjun.yarlagadda+13@bullhorn.com");
    }
        
    [StepDefinition("I enter Valid Password")]
    public void GivenIEnterValidPassword()
    {
        _loginPage.EnterPassword("Automation@Herefish");
    }
        
    [StepDefinition("I enter invalid username")]
    public void GivenIEnterInvalidUsername()
    {
        _loginPage.EnterUsername("test@te.co");
    }
        
    [StepDefinition("I enter invalid password")]
    public void GivenIEnterInvalidPassword()
    {
        _loginPage.EnterPassword("1rt124");
    }
        
    [StepDefinition("I enter invalid credentials")]
    public void GivenIEnterInvalidCredentials()
    {
        _loginPage.EnterUsername("testuser@rrr.com");
        _loginPage.EnterPassword("qwrwed@124");
        _loginPage.ClickLoginBtn();
    }
        
    [StepDefinition("I click on Forgot Password link")]
    public void GivenIClickOnForgotPasswordLink()
    {
        _loginPage.ClickForgotPassword();
    }
        
    [StepDefinition("I verify the Login Page is displayed")]
    public void ThenIVerifyTheLoginPageIsDisplayed()
    {
        _loginPage.VerifyLoginPageDisplayed();
    }
        
    [StepDefinition("I verify that I am successfully loged in")]
    public void ThenIVerifyThatIAmSuccessfullyLoggedIn()
    {
        _loginPage.ClickLoginBtn();
        _homePage.VerifyHomePageDisplayed();
    }
        
    [StepDefinition("I verify that I am not logged in")]
    public void ThenIVerifyThatIAmNotLoggedIn()
    {
        _loginPage.CheckErrorMessage();
    }
        
    [StepDefinition("I verify that the error message is displayed")]
    public void ThenIVerifyThatTheErrorMessageIsDisplayed()
    {
        _loginPage.ClickLoginBtn();
        Thread.Sleep(500);
        _loginPage.CheckErrorMessage();
    }
        
    [StepDefinition("I verify that the forgot password page is displayed")]
    public void ThenIVerifyThatTheForgotPasswordPageIsDisplayed()
    {
        _loginPage.VerifyForgotPasswordPage();
    }

    [When("I Login as User")]
    public void WhenILoginAsUser()
    {
        _loginPage.LoginAsUser();
    }

    [When("I Login as Admin")]
    public void WhenILoginAsAdmin()
    {
        _loginPage.LoginAsAdmin();
    }

    [When("I Login as Super User")]
    public void WhenILoginAsSuperUser()
    {
        _loginPage.LoginAsSuperUser();
    }
}
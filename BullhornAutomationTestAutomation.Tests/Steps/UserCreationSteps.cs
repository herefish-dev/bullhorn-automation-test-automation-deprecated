﻿using BullhornAutomationTestAutomation.Tests.PageObjects;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BullhornAutomationTestAutomation.Tests.Steps;

[Binding]
public class UserCreationSteps
{
    private IWebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    SettingsPage settingsPage;
    UsersPage usersPage;

    public UserCreationSteps(IWebDriver driver)
    {
        this.driver = driver;
        loginPage = new LoginPage(driver);
        homePage = new HomePage(driver);
        settingsPage = new SettingsPage(driver);
        usersPage = new UsersPage(driver);
    }
    [When(@"I click on Users Tab")]
    public void WhenIClickOnUsersTab()
    {
        settingsPage.ClickUsersTab();
    }
        
    [When(@"I click on Add Users Button")]
    public void WhenIClickOnAddUsersButton()
    {
        usersPage.ClickAddUsers();
    }
        
    [StepDefinition(@"I enter the new (.*) Info")]
    public void WhenIEnterTheNewRegularUserInfo(string user)
    {
            
    }
        
        
    [Then(@"I verify that the new user is successfully created")]
    public void ThenIVerifyThatTheNewUserIsSuccessfullyCreated()
    {
            
    }
}
﻿using System.Threading;
using BullhornAutomationTestAutomation.Tests.PageObjects;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BullhornAutomationTestAutomation.Tests.Steps;

[Binding]
public class PageLoadSmokeTestsSteps
{
    private IWebDriver _driver;
    LoginPage loginPage;
    HomePage homePage;
    DashboardsPage dashboardsPage;
    AutomationsPage automationsPage;
    EngagementsPage engagementsPage;
    ListsPage listsPage;
    ContactsPage contactsPage;
    LibraryPage libraryPage;
    SettingsPage settingsPage;
    ClientsPage clientsPage;

    public PageLoadSmokeTestsSteps(IWebDriver driver)
    {
        _driver = driver;
        loginPage = new LoginPage(driver);
        homePage = new HomePage(driver);
        dashboardsPage = new DashboardsPage(driver);
        automationsPage = new AutomationsPage(driver);
        engagementsPage = new EngagementsPage(driver);
        listsPage = new ListsPage(driver);
        contactsPage = new ContactsPage(driver);
        libraryPage = new LibraryPage(driver);
        settingsPage = new SettingsPage(driver);
        clientsPage = new ClientsPage(driver);
    }

    [StepDefinition("I verify that Dashboards page is loaded")]
    public void ThenIVerifyThatDashboardsPageIsLoaded()
    {
        dashboardsPage.VerifyDashboardPageIsDisplayed();
    }

    [Then("I verify that Dashboard Tabs are displayed")]
    public void ThenIVerifyThatDashboardTabsAreDisplayed()
    {
        dashboardsPage.VerifyDashboardTabsAreDisplayed();
    }
    
    [StepDefinition("I verify that Automations page is loaded")]
    public void ThenIVerifyThatAutomationsPageIsLoaded()
    {
        automationsPage.VerifyAutomationsPageIsDisplayed();
    }

    [StepDefinition("I click on Add Automation Button")]
    public void WhenIClickOnAddAutomationButton()
    {
        automationsPage.AddAutomationClick();
    }

    [StepDefinition("I verify that Add New Automation Drawer is displayed")]
    public void ThenIVerifyThatAddNewAutomationDrawerIsDisplayed()
    {
        automationsPage.VerifyAddAutomationDrawer();
    }
    
    [StepDefinition("I login to Herefish as Admin")]
    public void GivenILoginToHerefishDevAsAdmin()
    {
        loginPage.LoginAsAdmin();
        homePage.VerifyHomePageDisplayed();
    }
        
    [StepDefinition("I proceed to (.*) page")]
    public void WhenIProceedToSurveysPage(string page)
    {
        homePage.PageNav(page);
    }
        
    [StepDefinition("I click on Add Engagements Button")]
    public void WhenIClickOnAddSurveyButton()
    {
        engagementsPage.ClickAddNewEngagements();
    }
        
    [StepDefinition("I click on Add New List Button")]
    public void WhenIClickOnAddNewListButton()
    {
        listsPage.ClickNewListButton();
    }
        
    [StepDefinition("I click on Advanced Search Button")]
    public void WhenIClickOnAdvancedSearchButton()
    {
        contactsPage.ClickAdvancedSearchButton();
    }
        
    [StepDefinition("I click on Add Email Template Button")]
    public void WhenIClickOnAddEmailTemplateButton()
    {
        libraryPage.ClickAddEmailTemplate();
    }
        
    [StepDefinition("I verify that Engagements page is loaded")]
    public void ThenIVerifyThatSurveysPageIsLoaded()
    {
        engagementsPage.VerifyEngagementsPageIsDisplayed();
    }
        
    [StepDefinition("I verify that Add New Engagement Drawer is displayed")]
    public void ThenIVerifyThatAddNewSurveyDrawerIsDisplayed()
    {
        engagementsPage.VerifyAddEngagementDrawerDisplayed();
    }
        
    [StepDefinition("I verify that Lists page is loaded")]
    public void ThenIVerifyThatListsPageIsLoaded()
    {
        listsPage.VerifyListPageIsDisplayed();
    }
        
    [StepDefinition("I verify that Add New List Drawer is displayed")]
    public void ThenIVerifyThatAddNewListDrawerIsDisplayed()
    {
        listsPage.VerifyAddNewListDrawerIsDisplayed();
    }
        
    [StepDefinition("I verify that Contacts page is loaded")]
    public void ThenIVerifyThatContactsPageIsLoaded()
    {
        contactsPage.VerifyContactsPageIsDisplayed();
    }
        
    [StepDefinition("I verify that Advanced Search Drawer is displayed")]
    public void ThenIVerifyThatAdvancedSearchDrawerIsDisplayed()
    {
        contactsPage.VerifyAdvancedSearchDrawerIsDisplayed();
    }
        
    [StepDefinition("I verify that Library page is loaded")]
    public void ThenIVerifyThatLibraryPageIsLoaded()
    {
        libraryPage.VerifyLibraryPageDisplayed();
    }
        
    [StepDefinition("I verify that Email Template Drawer is displayed")]
    public void ThenIVerifyThatEmailTemplateDrawerIsDisplayed()
    {
        libraryPage.VerifyEmailTemplateDrawerIsDisplayed();
    }
        
    [StepDefinition("I verify that Settings page is displayed")]
    public void ThenIVerifyThatSettingsPageIsLoaded()
    {
        //homePage.NavigateToSettings();
        settingsPage.VerifySettingsPageIsDisplayed();
    }

    [Then("I verify that Settings page is not displayed")]
    public void ThenIVerifyThatSettingsPageIsNotDisplayed()
    {
        homePage.VerifySettingsTabIsNotDisplayed();
        settingsPage.VerifySettingsPageIsNotDisplayedThroughUrl();
    }

    [Then("I verify that Clients page is not displayed")]
    public void ThenIVerifyThatClientsPageIsNotDisplayed()
    {
        clientsPage.VerifyClientPageIsNotDisplayed();
        Thread.Sleep(2000);
        clientsPage.VerifyClientPageIsNotDisplayedThroughUrl();
    }

    [Then("I verify that Clients page is displayed")]
    public void ThenIVerifyThatClientsPageIsDisplayed()
    {
        homePage.NavigateToClientsPage();
        clientsPage.VerifyClientPageIsDisplayed();
    }

    [Then("I verify that I can change clients")]
    public void ThenIVerifyThatICanChangeClients()
    {
        homePage.VerifyICanChangeClientsSuperUser();
    }

    [Then("I verify that I can change between clients")]
    public void ThenIVerifyThatICanChangeBetweenClients()
    {
        homePage.ChangeBetweenClients();
    }
}
﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class LibraryPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;

    public LibraryPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement libraryPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title']"));
    private IWebElement emailTemplatesTab => _driver.FindElement(By.XPath("//*[@class='nav-tab-container']/ul[@class='nav hf-nav-tabs p-x-xl']/li[1]"));
    private IWebElement contentSourcesTab => _driver.FindElement(By.XPath("//*[@class='nav-tab-container']/ul[@class='nav hf-nav-tabs p-x-xl']/li[2]"));
    private IWebElement smartTokensTab => _driver.FindElement(By.XPath("//*[@class='nav-tab-container']/ul[@class='nav hf-nav-tabs p-x-xl']/li[3]"));
    private IWebElement addEmailTemplateButton => _driver.FindElement(By.XPath("//*[@class='container-fluid']/div[@class='row']/div[1]/div/a[@class='btn btn-hf-green']"));
    private IWebElement emailTemplateDrawerHeader => _driver.FindElement(By.XPath("//*[@class='drawer drawer-lg open']/form[@class='d-flex flex-column m-b-0 vh100 ng-pristine ng-valid']/div[@class='drawer-title']/h4"));
    private IWebElement fromTemplateButton => _driver.FindElement(By.XPath("//drawer-body[@class='ng-scope']/div[@class='row']/div[@class='col-md-6'][1]/div[@class='hf-panel hf-panel-hover']"));
    private IWebElement dragaAndDropButton => _driver.FindElement(By.XPath("//drawer-body[@class='ng-scope']/div[@class='row']/div[@class='col-md-6'][2]/div[@class='hf-panel hf-panel-hover']"));
    private IWebElement plainTextButton => _driver.FindElement(By.XPath("//drawer-body[@class='ng-scope']/div[@class='row']/div[@class='col-md-6'][3]/div[@class='hf-panel hf-panel-hover']"));
    public void ClickAddEmailTemplate() => addEmailTemplateButton.Click();

    public void VerifyLibraryPageDisplayed()
    {
        Assert.IsTrue(libraryPageHeader.Displayed);
        Assert.IsTrue(emailTemplatesTab.Displayed);
        Assert.IsTrue(contentSourcesTab.Displayed);
        Assert.IsTrue(smartTokensTab.Displayed);
    }

    public void VerifyEmailTemplateDrawerIsDisplayed()
    {
        Thread.Sleep(1000);
        Assert.IsTrue(emailTemplateDrawerHeader.Displayed);
        Assert.IsTrue(fromTemplateButton.Displayed);
        Assert.IsTrue(dragaAndDropButton.Displayed);
        Assert.IsTrue(plainTextButton.Displayed);
    }
}
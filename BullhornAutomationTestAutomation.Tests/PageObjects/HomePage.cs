﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class HomePage
{
    private IWebDriver driver;
    private WebDriverWait wait;
    public HomePage(IWebDriver driver)
    {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement bhImage => driver.FindElement(By.XPath("//*[@class='brand-img']"));
    private IWebElement leftNav => driver.FindElement(By.XPath("//*[@class='nav nav-sidebar m-b-lg']"));
    private IWebElement userProfile => driver.FindElement(By.XPath("//*[@class='nav navbar-nav navbar-right']/li[2]/a[@class='dropdown-toggle']"));
    private IWebElement Automations => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[2]"));
    private IWebElement Dashboard => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[1]"));
    private IWebElement Engagements => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[3]"));
    private IWebElement Lists => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[4]"));
    private IWebElement Contacts => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[5]"));
    private IWebElement Library => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[6]"));
    private IWebElement Settings => driver.FindElement(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[7]"));
    private IWebElement UserDropdown => driver.FindElement(By.XPath("//*[@class='nav navbar-nav navbar-right']/li[2]/a[@class='dropdown-toggle']"));
    private IWebElement TestSearch => driver.FindElement(By.XPath(""));
    private IWebElement ClientsOption => driver.FindElement(By.XPath("//*[@class='dropdown-menu dropdown-menu-default hf-dropdown-header-menu']/li[@class='ng-scope'][2]"));
    private IWebElement changeClientsOption => driver.FindElement(By.XPath("//*[@class='dropdown-menu dropdown-menu-default hf-dropdown-header-menu']/li[@class='ng-scope'][1]"));
    private IWebElement findClientTextBox => driver.FindElement(By.XPath("//*[@class='modal-body']/div[@class='form-group']/input[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']"));
    private IWebElement changeClientModalOption => driver.FindElement(By.XPath("//*[@class='modal-body']/div[@class='form-group']/ul/li/a"));
    private IWebElement changeClientModalSaveButton => driver.FindElement(By.XPath("//*[@class='modal-footer']/button[@class='btn btn-hf-blue']"));
    private IWebElement currentClientName => driver.FindElement(By.XPath("//*[@class='username username-hide-on-mobile hidden-xs hidden-sm ng-binding']/span[@class='ng-binding ng-scope']"));


    public void VerifyHomePageDisplayed()
    {
        wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='brand-img']")));
        Assert.IsTrue(bhImage.Displayed);
        Assert.IsTrue(leftNav.Displayed);
        Assert.IsTrue(userProfile.Displayed);
    }

    /// <summary>
    /// Clicks an icon on the left navigation bar
    /// </summary>
    /// <param name="page"></param>
    public void PageNav(string page)
    {

        string Pagename = page.ToLower();

        switch (Pagename)
        {
            case "dashboards":
                NavigateToDashboards();
                break;
            case "automations":
                NavigateToAutomations();
                break;
            case "engagements":
                NavigateToSurveys();
                break;
            case "lists":
                NavigateToLists();
                break;
            case "contacts":
                NavigateToContacts();
                break;
            case "library":
                NavigateToLibrary();
                break;
            case "settings":
                NavigateToSettings();
                break;
            case "test search":
                NavigateToTestSearch();
                break;
            case "clients":
                NavigateToClientsPage();
                break;


        }
    }

    public Boolean NavigateToDashboards()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[1]")));
        Thread.Sleep(2000);
        Dashboard.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='col-lg-6 col-md-12'][1]/div[@class='hf-panel']")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public Boolean NavigateToAutomations()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='inputSearchAutomations']")));
        Thread.Sleep(2000);
        Automations.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='hf-panel']/div[@class='table-responsive']/table[@class='table m-b-0 hf-table']/thead/tr/th[3]/span")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public Boolean NavigateToSurveys()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        Thread.Sleep(2000);
        Engagements.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='hf-panel']/div[@class='table-responsive']/table[@class='table hf-table table-hover']/thead/tr/th[1]/span")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public Boolean NavigateToLists()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        Thread.Sleep(2000);
        Lists.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='hf-panel']/div[@class='table-responsive']/table[@class='table m-b-0 hf-table']/thead/tr/th[2]/span")));
        Assert.IsTrue(SEelement.Displayed);
        return result;

    }
    public Boolean NavigateToContacts()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        Thread.Sleep(2000);
        Contacts.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='hf-panel']/div[@class='table-responsive']/table[@class='table hf-table m-b-0']/thead/tr/th[1]")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public Boolean NavigateToLibrary()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        Thread.Sleep(2000);
        Library.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='row']/div[1]/div[@class='btn-group pull-right m-b']/a[@class='btn btn-hf-green']")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public Boolean NavigateToSettings()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        Thread.Sleep(2000);
        Settings.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='row is-flex']/div[@class='col-md-4 flex-column']/div[@class='hf-panel'][1]")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public bool NavigateToTestSearch()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement LoadElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='input-group']/span[@class='input-group-btn']/button[@class='btn btn-default']")));
        UserDropdown.Click();
        Thread.Sleep(500);
        TestSearch.Click();
        IWebElement SEelement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='hf-panel-heading hf-panel-heading-main']/div[@class='hf-panel-title']")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    private bool IsElementPresent(By by)
    {
        try
        {
            driver.FindElement(by);
            return true;
        }
        catch (NoSuchElementException)
        {
            return false;
        }
    }

    public void VerifySettingsTabIsNotDisplayed()
    {
        if (IsElementPresent(By.XPath("//*[@class='sidebar']/ul[@class='nav nav-sidebar m-b-lg']/li[7]")))
        {
            Assert.IsTrue(false, "Settings Tab is Displayed when it shouldn't be.");
        }
        else
        {
            Assert.IsTrue(true, "Settings Tab is not Displayed as expected.");
        }
    }
    public Boolean NavigateToClientsPage()
    {
        bool result = false;
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        UserDropdown.Click();
        Thread.Sleep(500);
        ClientsOption.Click();
        IWebElement SEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='container-fluid']/div[@class='row'][2]/div[@class='hf-panel']")));
        Assert.IsTrue(SEelement.Displayed);
        return result;
    }

    public void VerifyICanChangeClientsSuperUser()
    {
        UserDropdown.Click();
        Thread.Sleep(500);
        changeClientsOption.Click();
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement sEelement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='modal-body']/div[@class='form-group']/input[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']")));
        Assert.IsTrue(sEelement.Displayed);
        findClientTextBox.Clear();
        findClientTextBox.SendKeys("XYZ Staffing");
        changeClientModalOption.Click();
        changeClientModalSaveButton.Click();
        Thread.Sleep(5000);
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement selement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='nav nav-sidebar m-b-lg']")));
        string clientName = currentClientName.Text.Trim();
        //Assert.AreEqual(clientName, "(XYZ Staffing)");
        UserDropdown.Click();
        Thread.Sleep(500);
        changeClientsOption.Click();
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement selement1 = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='modal-body']/div[@class='form-group']/input[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']")));
        findClientTextBox.Clear();
        findClientTextBox.SendKeys("ABC Staffing");
        changeClientModalOption.Click();
        changeClientModalSaveButton.Click();
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement selement2 = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='nav nav-sidebar m-b-lg']")));
        Thread.Sleep(5000);
    }

    public void ChangeBetweenClients()
    {
        UserDropdown.Click();
        Thread.Sleep(500);
        changeClientsOption.Click();
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement sElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='modal-body']/div[@class='form-group']/select[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']")));
        Assert.IsTrue(sElement.Displayed);
        IWebElement clientDropdown = driver.FindElement(By.XPath("//*[@class='modal-body']/div[@class='form-group']/select[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']"));
        clientDropdown.Click();
        SelectElement dropDown = new SelectElement(driver.FindElement(By.XPath("/html/body/div[1]/div/div/form/div[2]/div[1]/select")));
        dropDown.SelectByText("XYZ Staffing");
        changeClientModalSaveButton.Click();
        Thread.Sleep(5000);
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        sElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='nav nav-sidebar m-b-lg']")));
        string clientName = currentClientName.Text.Trim();
        //Assert.AreEqual(clientName, "(XYZ Staffing)");
        UserDropdown.Click();
        Thread.Sleep(500);
        changeClientsOption.Click();
        driver.SwitchTo().Window(driver.WindowHandles.Last());
        IWebElement sElement1 = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='modal-body']/div[@class='form-group']/select[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']")));
        IWebElement clientDropdown1 = driver.FindElement(By.XPath("//*[@class='modal-body']/div[@class='form-group']/select[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']"));
        clientDropdown1.Click();
        SelectElement dropDown1 = new SelectElement(driver.FindElement(By.XPath("/html/body/div[1]/div/div/form/div[2]/div[1]/select")));
        dropDown1.SelectByText("ABC Staffing");
        changeClientModalSaveButton.Click();
        Thread.Sleep(5000);
        driver.SwitchTo().Window(driver.WindowHandles.Last());
    }
}
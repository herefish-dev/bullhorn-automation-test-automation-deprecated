﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class AutomationsPage
{
    private IWebDriver driver;
    private WebDriverWait wait;
    public AutomationsPage(IWebDriver driver)
    {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement automationsPageHeader => driver.FindElement(By.XPath("//*[@class='page-header-title flex-grow-1']"));
    private IWebElement viewMetricsButton => driver.FindElement(By.XPath("//*[@class='page-header']/a[@class='btn btn-default m-r-lg']"));
    private IWebElement listViewButton => driver.FindElement(By.XPath("//*[@class='btn-group btn-group-justified map-toggle m-r-lg']/label[@class='btn btn-default active']"));
    private IWebElement mapViewButton => driver.FindElement(By.XPath("//*[@class='btn-group btn-group-justified map-toggle m-r-lg']/label[@class='btn btn-default']"));
    private IWebElement addAutomationButton => driver.FindElement(By.Id("buttonAddAutomation"));
    private IWebElement blankAutomationButton => driver.FindElement(By.Id("divBlankAutomation"));
    private IWebElement addAutoDrawerHeader => driver.FindElement(By.XPath("//*[@class='d-flex flex-column m-b-0 vh100 ng-pristine ng-invalid ng-invalid-required ng-valid-unique']/div[@class='drawer-title']/h4[@class='m-a-0 ng-binding']"));
    private string addAutoHeaderText => addAutoDrawerHeader.Text.Trim();
    public void AddAutomationClick() => addAutomationButton.Click();
    public void ClickViewMetrics() => viewMetricsButton.Click();
    public void ClickMapView() => mapViewButton.Click();
    public void VerifyAutomationsPageIsDisplayed()
    {
        Assert.IsTrue(automationsPageHeader.Displayed);
        Assert.IsTrue(viewMetricsButton.Displayed);
        Assert.IsTrue(listViewButton.Displayed);
        Assert.IsTrue(addAutomationButton.Displayed);
    }

    public void VerifyAddAutomationDrawer()
    {
        Assert.IsTrue(blankAutomationButton.Displayed);
        Assert.IsTrue(addAutoDrawerHeader.Displayed);
        Assert.AreEqual(addAutoHeaderText, "Add New Automation");
    }
}
﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class UsersPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;

    public UsersPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement usersTabHeader => _driver.FindElement(By.XPath("//hng-users[@class='ng-isolate-scope']/div[@class='ng-scope']/div[@class='row']/div[@class='col-sm-8']/h4[@class='p-t']"));
    
    private IWebElement addUsersButton => _driver.FindElement(By.Id("btnAddUser"));

    public void ClickAddUsers() => addUsersButton.Click();
}
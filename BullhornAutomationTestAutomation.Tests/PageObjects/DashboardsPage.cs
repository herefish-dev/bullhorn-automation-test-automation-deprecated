﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class DashboardsPage
{
    private readonly IWebDriver _driver;
    private readonly WebDriverWait _wait;

    public DashboardsPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement dashboardPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header-title']"));
    private IWebElement overviewTab => _driver.FindElement(By.XPath("//*[@class='nav hf-nav-tabs p-x-xl']/li[1]"));
    private IWebElement candidatesTab => _driver.FindElement(By.XPath("//*[@class='nav hf-nav-tabs p-x-xl']/li[2]"));
    private IWebElement salesTab => _driver.FindElement(By.XPath("//*[@class='nav hf-nav-tabs p-x-xl']/li[3]"));
    private IWebElement contractorsTab => _driver.FindElement(By.XPath("//*[@class='nav hf-nav-tabs p-x-xl']/li[1]"));
    private IWebElement automationActionsHeading => _driver.FindElement(By.XPath("//*[@class='flex-wrap is-flex row'][1]/div[@class='col-lg-6 col-md-12'][2]/div[@class='hf-panel']/div[@class='hf-panel-heading hf-panel-heading-main']"));

    public void VerifyDashboardPageIsDisplayed()
    {
        _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='page-header-title']")));
        Assert.IsTrue(overviewTab.Displayed, "The Overview page is not displayed as expected.");
        Assert.IsTrue(automationActionsHeading.Displayed, "Automation Actions Tile is not displayed as expected.");
    }

    public void VerifyDashboardTabsAreDisplayed()
    {
        Assert.IsTrue(overviewTab.Displayed, "The Overview Tab is not displayed as expected.");
        Assert.IsTrue(candidatesTab.Displayed, "The Candidates Tab is not displayed as expected.");
        Assert.IsTrue(salesTab.Displayed, "The Sales Tab is not displayed as expected.");
        Assert.IsTrue(contractorsTab.Displayed, "The Contractors Tab is not displayed as expected.");
    }
}
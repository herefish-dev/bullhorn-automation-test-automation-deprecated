﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class SettingsPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;
    readonly LoginPage _loginPage;

    public SettingsPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
        _loginPage = new LoginPage(driver);
    }

    private IWebElement settingPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title']"));
    private IWebElement companySettingsTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[1]"));
    private IWebElement atsTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[2]"));
    private IWebElement ownerAssignmentRulesTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[3]"));
    private IWebElement websiteIntegrationsTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[4]"));
    private IWebElement usersTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[5]"));
    private IWebElement apiTab => _driver.FindElement(By.XPath("//*[@id='divSettingsTab']/ul[@class='nav hf-nav-tabs p-x-xl']/li[6]"));
    private IWebElement generalTile => _driver.FindElement(By.XPath("//*[@class='col-md-4 flex-column']/div[@class='hf-panel'][1]"));
    private IWebElement emailFooterTile => _driver.FindElement(By.XPath("//*[@class='col-md-4 flex-column']/div[@class='hf-panel'][2]"));
    public void ClickUsersTab() => usersTab.Click();
    
    public void VerifySettingsPageIsDisplayed()
    {
        companySettingsTab.Click();
        Assert.IsTrue(settingPageHeader.Displayed);
        Assert.IsTrue(companySettingsTab.Displayed);
        Assert.IsTrue(atsTab.Displayed);
        Assert.IsTrue(websiteIntegrationsTab.Displayed);
        Assert.IsTrue(apiTab.Displayed);
        Assert.IsTrue(generalTile.Displayed);
        Assert.IsTrue(emailFooterTile.Displayed);
    }

    public void VerifySettingsPageIsNotDisplayed()
    {
        Assert.IsFalse(settingPageHeader.Displayed);
        Assert.IsFalse(companySettingsTab.Displayed);
        Assert.IsFalse(atsTab.Displayed);
        Assert.IsFalse(websiteIntegrationsTab.Displayed);
        Assert.IsFalse(apiTab.Displayed);
        Assert.IsFalse(generalTile.Displayed);
        Assert.IsFalse(emailFooterTile.Displayed);
    }

    public void VerifySettingsPageIsNotDisplayedThroughUrl()
    {
        _driver.Navigate().GoToUrl("https://test-herefish-web.azurewebsites.net/Settings/Settings");
        Thread.Sleep(3000);
        Assert.IsTrue(_loginPage.VerifyLoginPageDisplayed(), "The User can access the clients page.");
    }
}
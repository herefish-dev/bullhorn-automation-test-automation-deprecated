﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class EngagementsPage
{
    private IWebDriver driver;
    private WebDriverWait wait;
    public EngagementsPage(IWebDriver driver)
    {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement engagementsPageHeader => driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title']"));
    private string pageHeaderTxt => engagementsPageHeader.Text.Trim();
    private IWebElement addNewEngagementBtn => driver.FindElement(By.Id("btnAddNewSurvey"));
    private string addNewBtnTxt => addNewEngagementBtn.Text.Trim();
    private IWebElement searchTextBox => driver.FindElement(By.Id("inputSearchSurveys"));
    private IWebElement searchButton => driver.FindElement(By.XPath("//*[@class='input-group-btn']/button[@class='btn btn-default']"));
    private IWebElement addEngagementDrawerHeader => driver.FindElement(By.XPath("//form[@class='d-flex flex-column m-b-0 vh100 ng-pristine ng-invalid ng-invalid-required ng-valid-sanitized']/div[@class='drawer-title']/h4"));
    private string addEngagementDrawerText => addEngagementDrawerHeader.Text.Trim();
    private IWebElement blankEngagementButton => driver.FindElement(By.XPath("//*[@class='hf-panel hf-panel-hover d-flex flex-column']/div[@class='hf-panel-body flex-grow-1']"));
    public void ClickAddNewEngagements() => addNewEngagementBtn.Click();
    public void VerifyEngagementsPageIsDisplayed()
    {
        Assert.IsTrue(engagementsPageHeader.Displayed);
        Assert.IsTrue(addNewEngagementBtn.Displayed);
        Assert.IsTrue(searchButton.Displayed);
        Assert.IsTrue(searchButton.Displayed);
    }

    public void VerifyAddEngagementDrawerDisplayed()
    {
        Thread.Sleep(2000);
        Assert.IsTrue(addEngagementDrawerHeader.Displayed);
        Assert.IsTrue(blankEngagementButton.Displayed);
        Assert.AreEqual(addEngagementDrawerText, "Add Engagement");
    }


}
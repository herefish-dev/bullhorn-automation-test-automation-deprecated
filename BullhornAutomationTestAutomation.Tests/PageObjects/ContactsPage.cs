﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class ContactsPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;

    public ContactsPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement contactsPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title']"));
    private IWebElement selectListDropdown => _driver.FindElement(By.XPath("//*[@class='multiSelect inlineBlock']/button[@class='ng-binding']"));
    private IWebElement advancedSearchButton => _driver.FindElement(By.XPath("//*[@class='col-sm-4 col-lg-6']/div/button[@class='btn btn-default btn-sm pull-right']"));
    private IWebElement listTable => _driver.FindElement(By.XPath("//*[@class='row ng-scope']/div[@class='hf-panel']"));
    public void ClickAdvancedSearchButton() => advancedSearchButton.Click();
    private IWebElement advancedDrawerHeader => _driver.FindElement(By.XPath("//*[@class='drawer-title']/h4[@class='m-a-0 ng-binding']"));
    private IWebElement candidatesRadioButton => _driver.FindElement(By.XPath("//*[@class='m-t']/div[@class='form-group']/div[@class='form-inline row']/div[@class='form-group col-sm-4'][1]/label"));

    public void VerifyContactsPageIsDisplayed()
    {
        Assert.IsTrue(contactsPageHeader.Displayed);
        Assert.IsTrue(selectListDropdown.Displayed);
        Assert.IsTrue(listTable.Displayed);
    }

    public void VerifyAdvancedSearchDrawerIsDisplayed()
    {
        Assert.IsTrue(advancedDrawerHeader.Displayed);
        Assert.IsTrue(candidatesRadioButton.Displayed);
    }
}
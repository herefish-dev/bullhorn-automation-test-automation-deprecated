﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class LoginPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;

    public LoginPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement usernameTextBox => _driver.FindElement(By.XPath("//*[@class='form-group'][1]/input"));
    private IWebElement passwordTextBox => _driver.FindElement(By.XPath("//*[@class='form-group'][2]/input"));
    private IWebElement loginButton => _driver.FindElement(By.XPath("//*[@class='form-group'][3]/button"));
    private IWebElement rememberMeChkBx => _driver.FindElement(By.XPath("//*[@class='form-group m-b-lg']/label"));
    private IWebElement forgotPasswordLink => _driver.FindElement(By.XPath("//*[@class='text-center p-t']/a"));
    private IWebElement errorMessage => _driver.FindElement(By.XPath("//*[@class='well well-danger']/div[@class='ng-binding']"));
    private IWebElement resetPageHeader => _driver.FindElement(By.XPath("//*[@class='login-form_wrap']/div[@class='h2 hf-font-med']"));
    private IWebElement emailTextBox => _driver.FindElement(By.XPath("//*[@class='form-control input-lg ng-pristine ng-untouched ng-valid ng-empty']"));
    private IWebElement resetButton => _driver.FindElement(By.XPath("//*[@class='btn btn-hf-green btn-block btn-lg']"));
    private string errorMsgText => errorMessage.Text.Trim();
    public void ClickForgotPassword() => forgotPasswordLink.Click();
    public void ClickLoginBtn() => loginButton.Click();
    private string resetPageHeaderTxt => resetPageHeader.Text.Trim();


    public void CheckErrorMessage()
    {
        Thread.Sleep(500);
        Assert.IsTrue(errorMessage.Displayed);
        Assert.AreEqual(errorMsgText, "Invalid email address or password");
    }
    public void EnterUsername(string userName)
    {
        usernameTextBox.Clear();
        usernameTextBox.SendKeys(userName);
    }

    public void EnterPassword(string password)
    {
        passwordTextBox.Clear();
        passwordTextBox.SendKeys(password);
    }

    public void LoginAsAdmin()
    {
        usernameTextBox.Clear();
        usernameTextBox.SendKeys("reino.boonstra+07@bullhorn.com");
        passwordTextBox.Clear();
        passwordTextBox.SendKeys("buckrogers");
        loginButton.Click();
    }

    public void LoginAsSuperUser()
    {
        usernameTextBox.Clear();
        usernameTextBox.SendKeys("reino.boonstra+05@bullhorn.com");
        passwordTextBox.Clear();
        passwordTextBox.SendKeys("buckrogers");
    }

    public void LoginAsUser()
    {
        usernameTextBox.Clear();
        usernameTextBox.SendKeys("reino.boonstra+06@bullhorn.com");
        passwordTextBox.Clear();
        passwordTextBox.SendKeys("buckrogers");
    }

    public void VerifyForgotPasswordPage()
    {
        Assert.IsTrue(resetButton.Displayed);
        Assert.IsTrue(emailTextBox.Displayed);
        Assert.AreEqual(resetPageHeaderTxt, "Need help with your password?");
    }

    public bool VerifyLoginPageDisplayed()
    {
        try
        {

            Assert.IsTrue(usernameTextBox.Displayed);
            Assert.IsTrue(passwordTextBox.Displayed);
            Assert.IsTrue(loginButton.Displayed);

            return true;
        }
        catch (NoSuchElementException)
        {
            return false;
        }
    }
}
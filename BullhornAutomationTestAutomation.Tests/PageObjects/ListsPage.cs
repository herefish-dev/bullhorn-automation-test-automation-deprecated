﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class ListsPage
{
    private readonly IWebDriver _driver;
    private WebDriverWait _wait;

    public ListsPage(IWebDriver driver)
    {
        _driver = driver;
        _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
    }

    private IWebElement listsPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title flex-grow-1']"));
    private IWebElement addNewListBtn => _driver.FindElement(By.Id("btnAddNewList"));
    private IWebElement selectGroupsDropdown => _driver.FindElement(By.XPath("//*[@class='multiSelect inlineBlock']/button[@class='ng-binding']"));
    private IWebElement listSearchBox => _driver.FindElement(By.Id("inputSearchLists"));
    private IWebElement listSearchButton => _driver.FindElement(By.XPath("//*[@class='input-group-btn']/button[@class='btn btn-default']"));
    private IWebElement listDrawerHeader => _driver.FindElement(By.XPath("//*[@class='drawer drawer-lg open']/form/div[@class='drawer-title']/h4"));
    private IWebElement candidateBasedOption => _driver.FindElement(By.Id("divCandidateBased"));
    private IWebElement salesContactBasedOption => _driver.FindElement(By.Id("divSalesContactBased"));
    private IWebElement submissionBasedOption => _driver.FindElement(By.Id("divSubmissionBased"));
    private IWebElement placementBasedOption => _driver.FindElement(By.Id("divPlacementBased"));
    private IWebElement jobBasedOption => _driver.FindElement(By.Id("divJobBased"));
    public void ClickNewListButton() => addNewListBtn.Click();

    public void VerifyListPageIsDisplayed()
    {
        Assert.IsTrue(listsPageHeader.Displayed);
        Assert.IsTrue(addNewListBtn.Displayed);
        Assert.IsTrue(listSearchBox.Displayed);
        Assert.IsTrue(listSearchButton.Displayed);

    }

    public void VerifyAddNewListDrawerIsDisplayed()
    {
        Thread.Sleep(1000);
        Assert.IsTrue(listDrawerHeader.Displayed);
        Assert.IsTrue(candidateBasedOption.Displayed);
        Assert.IsTrue(salesContactBasedOption.Displayed);
        Assert.IsTrue(jobBasedOption.Displayed);

    }
}
﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace BullhornAutomationTestAutomation.Tests.PageObjects;

public class ClientsPage
{
    private readonly IWebDriver _driver;

    public ClientsPage(IWebDriver driver)
    {
        _driver = driver;
    }

    private IWebElement clientsSearchBox => _driver.FindElement(By.XPath("//*[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']"));

    private IWebElement clientsPageHeader => _driver.FindElement(By.XPath("//*[@class='page-header']/h3[@class='page-header-title']"));

    private IWebElement addNewClientButton => _driver.FindElement(By.XPath("//*[@class='ng-scope']/div[@class='ng-scope']/div[@class='page-header']/button[@class='btn btn-hf-blue']"));

    public void ClickAddNewClient() => addNewClientButton.Click();

    private IWebElement clientsSearchButton => _driver.FindElement(By.XPath("//*[@class='input-group-btn']/button[@class='btn btn-default']"));

    public void VerifyClientPageIsDisplayed()
    {
        Assert.IsTrue(clientsPageHeader.Displayed);
        Assert.IsTrue(clientsSearchButton.Displayed);
        Assert.IsTrue(addNewClientButton.Displayed);
    }

    public void VerifyClientPageIsNotDisplayed()
    {
        Assert.IsFalse(clientsPageHeader.Displayed);
        Assert.IsFalse(clientsSearchButton.Displayed);
        Assert.IsFalse(addNewClientButton.Displayed);
    }

    public void VerifyClientPageIsNotDisplayedThroughUrl()
    {
        _driver.Navigate().GoToUrl("https://test-herefish-web.azurewebsites.net/Customers/Customers");
        Thread.Sleep(2000);
        Assert.IsFalse(clientsPageHeader.Enabled);
        Assert.IsFalse(clientsSearchButton.Displayed);
        Assert.IsFalse(addNewClientButton.Displayed);
    }
}
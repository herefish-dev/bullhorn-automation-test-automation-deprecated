﻿Feature: PageLoadSmokeTests

@Smoke @Regression @PageLoad
Scenario: 2.1_Verify Dashboards Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Dashboards page
	Then I verify that Dashboards page is loaded
	And I verify that Dashboard Tabs are displayed
	
@Smoke @Regression @PageLoad
Scenario: 2.2_Verify Automations Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Automations page
	Then I verify that Automations page is loaded
	When I click on Add Automation Button
	Then I verify that Add New Automation Drawer is displayed

@Smoke @Regression @PageLoad
Scenario: 2.3_Verify Engagments Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Engagements page
	Then I verify that Engagements page is loaded
	When I click on Add Engagements Button
	Then I verify that Add New Engagement Drawer is displayed

@Smoke @Regression @PageLoad
Scenario: 2.4_Verify Lists Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Lists page
	Then I verify that Lists page is loaded
	When I click on Add New List Button
	Then I verify that Add New List Drawer is displayed

@Smoke @Regression @PageLoad
Scenario: 2.5_Verify Contacts Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Contacts page
	Then I verify that Contacts page is loaded
	When I click on Advanced Search Button
	Then I verify that Advanced Search Drawer is displayed

@Smoke @Regression @PageLoad
Scenario: 2.6_Verify Library Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Library page
	Then I verify that Library page is loaded
	When I click on Add Email Template Button
	Then I verify that Email Template Drawer is displayed

@Smoke @Regression @PageLoad
Scenario: 2.7_Verify Settings Page is Loading
	Given I login to Herefish as Admin
	When I proceed to Settings page
	Then I verify that Settings page is displayed
	


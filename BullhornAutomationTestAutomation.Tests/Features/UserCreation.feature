﻿Feature: UserCreation


@UserPrivileges @Regression
Scenario: 4.1_Creating A Regular User As Admin
	Given I navigate to Herefish
	When I Login as Admin
	And I verify that I am successfully loged in
	And I proceed to Settings page
	And I click on Users Tab
	And I click on Add Users Button
	When I enter the new regular user Info
	Then I verify that the new user is successfully created

@UserPrivileges @Smoke @Regression
Scenario: 4.2_Creating A Admin User As Admin
	Given I navigate to Herefish
	When I Login as Admin
	And I verify that I am successfully loged in
	And I proceed to Settings page
	And I click on Users Tab
	And I click on Add Users Button
	When I enter the new admin user Info
	Then I verify that the new user is successfully created

@UserPrivileges @Smoke @Regression
Scenario: 4.3_Creating A Regular User As Super User
	Given I navigate to Herefish
	When I Login as Admin
	And I verify that I am successfully loged in
	And I proceed to Settings page
	And I click on Users Tab
	And I click on Add Users Button
	When I enter the new regular user Info
	Then I verify that the new user is successfully created

@UserPrivileges @Smoke @Regression
Scenario: 4.4_Creating A Admin User As Super User
	Given I navigate to Herefish
	When I Login as Admin
	And I verify that I am successfully loged in
	And I proceed to Settings page
	And I click on Users Tab
	And I click on Add Users Button
	When I enter the new admin user Info
	Then I verify that the new user is successfully created


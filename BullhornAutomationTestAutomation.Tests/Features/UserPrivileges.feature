﻿Feature: UserPrivileges
	This Feature tests the user privileges

@UserPrivileges @Smoke @Regression
Scenario: 3.1_Login as User
	Given I navigate to Herefish
	When I Login as User
	Then I verify that I am successfully loged in

@UserPrivileges @Smoke @Regression
Scenario: 3.2_Login as Admin
	Given I navigate to Herefish
	When I Login as Admin
	Then I verify that I am successfully loged in

@UserPrivileges @Smoke @Regression
Scenario: 3.3_Login as Super User
	Given I navigate to Herefish
	When I Login as Super User
	Then I verify that I am successfully loged in

@UserPrivileges @Smoke @Regression
Scenario: 3.4_Verify User Role Privileges
	Given I navigate to Herefish
	When I Login as User
	Then I verify that I am successfully loged in
	And I verify that Settings page is not displayed

@UserPrivileges @Smoke @Regression
Scenario: 3.5_Login as Admin
	Given I navigate to Herefish
	When I Login as Admin
	Then I verify that I am successfully loged in
	And I proceed to Settings page
	And I verify that Settings page is displayed
	

@UserPrivileges @Smoke @Regression
Scenario: 3.6_Login as Super User
	Given I navigate to Herefish
	When I Login as Super User
	Then I verify that I am successfully loged in
	And I proceed to Settings page
	And I verify that Settings page is displayed
	And I verify that Clients page is displayed

@UserPrivileges @Smoke @Regression
Scenario: 3.7_Verify Super User Can Change Clients
	Given I navigate to Herefish
	When I Login as Super User
	Then I verify that I am successfully loged in
	And I verify that I can change clients

@UserPrivileges @Smoke @Regression
Scenario: 3.8_Verify Admin Can Change Clients
	Given I navigate to Herefish
	When I Login as Admin
	Then I verify that I am successfully loged in
	And I verify that I can change between clients

@UserPrivileges @Smoke @Regression
Scenario: 3.8_Verify User Can Change Clients
	Given I navigate to Herefish
	When I Login as User
	Then I verify that I am successfully loged in
	And I verify that I can change between clients

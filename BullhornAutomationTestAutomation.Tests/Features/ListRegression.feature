﻿Feature: 6_ListsRegression

@Lists @Smoke @Regression
Scenario Outline: 6.1_Add a Single Condition List
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Lists page
	When I click on Add New List
	Then I verify that Add new List drawer is displayed
	When I select record type <Entity Type>
	Then I verify that List Name drawer is displayed
	When I enter the name of the List as <List Name>
	And I select Standard Radio button
	And I click on Save button
	Then I verify that List is successfully saved
	And I verify that the user is directed to the newly created List page
	Examples: 
	| Entity Type         | List Name                    |
	| Candidate-based     | Candidate Based List TAG     |
	| Sales Contact-based | Sales Contact Based List TAG |
	| Submission-based    | Submission Based List TAG    |
	| Placement-based     | Placement Based List TAG     |
	| Job-based           | Job Based List TAG           |

@Lists @Smoke @Regression
Scenario Outline: 3.2_Edit a List
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Lists page
	When I search for <List Name> List
	Then I verify the List is displayed in the list
	When I click on the selected List
	Then I verify that List is loaded	
	Examples: 
	| Entity Type         | List Name                    |
	| Candidate-based     | Candidate Based List TAG     |
	| Sales Contact-based | Sales Contact Based List TAG |
	| Submission-based    | Submission Based List TAG    |
	| Placement-based     | Placement Based List TAG     |
	| Job-based           | Job Based List TAG           |

	
@Lists @Smoke @Regression
Scenario Outline: 3.3_Deleting aList
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Lists page
	When I search for <List Name> List
	Then I verify the List is displayed in the list
	When I select Delete from List Actions Dropdown
	Then I verify that the selected List is Deleted
	Examples: 
	| List Name                    |
	| Candidate Based List TAG     |
	| Sales Contact Based List TAG |
	| Submission Based List TAG    |
	| Placement Based List TAG     |
	| Job Based List TAG           |
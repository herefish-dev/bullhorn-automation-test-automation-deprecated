﻿Feature: AutomationRegression
	This feature tests the Automation page. Adding a new automation, editing an exisiting one and deleting. 


@Automation @Smoke @Regression
Scenario Outline: 3.1_Add a blank automation
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Automations page
	When I click on Add Automation
	Then I verify that Add new Automation drawer is displayed
	When I select Blank Automation
	Then I verify that Choose Record Type drawer is displayed
	When I select record type <Entity Type>
	Then I verify that Name your Automation drawer is displayed
	When I enter the name of the Automation as <Automation Name>
	And I select Standard Radio button
	And I click on Save button
	Then I verify that Automation is successfully saved
	And I verify that the user is directed to the newly created automation page
	And I verify that the Automation Enrollment dialog box is displayed
	And I verify that the Automation is Paused
	Examples: 
	| Entity Type         | Automation Name         |
	| Candidate-based     | Candidate Based TAG     |
	| Sales Contact-based | Sales Contact Based TAG |
	| Submission-based    | Submission Based TAG    |
	| Placement-based     | Placement Based TAG     |
	| Job-based           | Job Based TAG           |

@Automation @Smoke @Regression
Scenario Outline: 3.2_Edit an Automation
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Automations page
	When I search for <Automation Name> Automation
	Then I verify the Automation is displayed in the list
	When I click on the selected Automation
	Then I verify that Automation is loaded
	When I click on Add button
	Then I verify that Add New Step Modal is displayed
	When I click on Add Wait option
	Then I verify that Wait step is added to the automation
	When I enter the values and save the wait step
	Then I verify that wait step is successfully saved
	When I click on Turn on Automation
	Then I verify that the Automation is turned on
	
	Examples: 
	| Entity Type         | Automation Name         |
	| Candidate-based     | Candidate Based TAG     |
	| Submission-based    | Submission Based TAG    |
	| Placement-based     | Placement Based TAG     |
	| Job-based           | Job Based TAG           |

	
@Automation @Smoke @Regression
Scenario Outline: 3.3_Deleting an Automation
	Given I navigate to Herefish
	When I Login as Super User
	And I verify that Homepage is displayed
	And I proceed to Automations page
	When I search for <Automation Name> Automation
	Then I verify the Automation is displayed in the list
	When I select Delete from Actions Dropdown
	Then I verify that the selected Automation is Deleted
	Examples: 
	| Automation Name         |
	| Candidate Based TAG     |
	| Sales Contact Based TAG |
	| Submission Based TAG    |
	| Placement Based TAG     |
	| Job Based TAG           |
﻿Feature: Login
@LogIn @Smoke @Regression
Scenario: 1.1_Verifying Login PageObjects
	Given I navigate to Herefish
	Then I verify the Login Page is displayed

@LogIn @Smoke @Regression @Staging @Prod
Scenario: 1.2_Login with valid credentials	
	Given I navigate to Herefish
	And I enter valid Username
	And I enter Valid Password
	Then I verify that I am successfully loged in 

@LogIn @Smoke @Regression
Scenario: 1.3_Login with Invalid Credentials
	Given I navigate to Herefish
	When I enter invalid username
	And I enter invalid password
	Then I verify that the error message is displayed

@LogIn @Smoke @Regression
Scenario: 1.4_Verifying the error message
	Given I navigate to Herefish
	And I enter invalid credentials
	Then I verify that the error message is displayed

@LogIn @Smoke @Regression
Scenario: 1.5_Verifying Forgot Password
	Given I navigate to Herefish
	And I click on Forgot Password link
	Then I verify that the forgot password page is displayed

@LogIn @Smoke @Regression
Scenario: 1.6_Login as User
	Given I navigate to Herefish
	When I Login as User
	Then I verify that I am successfully loged in

@LogIn @Smoke @Regression
Scenario: 1.7_Login as Admin
	Given I navigate to Herefish
	When I Login as Admin
	Then I verify that I am successfully loged in

@LogIn @Smoke @Regression
Scenario: 1.8_Login as Super User
	Given I navigate to Herefish
	When I Login as Super User
	Then I verify that I am successfully loged in



